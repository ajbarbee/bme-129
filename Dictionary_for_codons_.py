#AA acid Dictonary and Values

AANameTo3Dic = {'Alanine':'Ala', 'Arginine':'Arg','Asparagine':'Asn','Aspartate':'Asp','Cysteine':'Cys','Glutamate':'Glu','Glutamine':'Gln',
              'Glycine':'Gly','Histidine':'His','Isoleucine':'Ile','Leucine':'Leu','Lysine':'Lys','Methionine':'Met','Phenylalanine':'Phe',
              'Proline':'Pro','Serine':'Ser','Threonine':'Thr','Tryptophan':'Trp','Tyrosine':'Tyr','Valine':'Val'}

AA3ToNameDic = {'Ala': 'Alanine', 'Arg': 'Arginine', 'Asn': 'Asparagine', 'Asp': 'Aspartate', 'Cys': 'Cysteine', 'Glu': 'Glutamate', 'Gln': 'Glutamine',
                'Gly': 'Glycine', 'His': 'Histidine', 'Ile': 'Isoleucine', 'Leu': 'Leucine', 'Lys': 'Lysine', 'Met': 'Methionine', 'Phe': 'Phenylalanine',
                'Pro': 'Proline', 'Ser': 'Serine', 'Thr': 'Threonine', 'Trp': 'Tryptophan', 'Tyr': 'Tyrosine', 'Val': 'Valine'}

AANameTo1Dic = {'Alanine':'A', 'Arginine':'R','Asparagine':'N','Aspartate':'D','Cysteine':'C','Glutamate':'E','Glutamine':'Q',
              'Glycine':'G','Histidine':'H','Isoleucine':'I','Leucine':'L','Lysine':'K','Methionine':'M','Phenylalanine':'F',
              'Proline':'P','Serine':'S','Threonine':'T','Tryptophan':'W','Tyrosine':'Y','Valine':'V'}

AA1ToNameDic = {'A': 'Alanine', 'R': 'Arginine', 'N': 'Asparagine', 'D': 'Aspartate', 'C': 'Cysteine', 'E': 'Glutamate', 'Q': 'Glutamine',
                'G': 'Glycine', 'H': 'Histidine', 'I': 'Isoleucine', 'L': 'Leucine', 'K': 'Lysine', 'M': 'Methionine', 'F': 'Phenylalanine',
                'P': 'Proline', 'S': 'Serine', 'T': 'Threonine', 'W': 'Tryptophan', 'Y': 'Tyrosine', 'V': 'Valine'}

AAWeightDic = {'Alanine':89.1, 'Arginine':174.2,'Asparagine':132.1,'Aspartate':133.1,'Cysteine':121.2,'Glutamate':147.1,'Glutamine':146.2,
              'Glycine':75.1,'Histidine':155.2,'Isoleucine':131.2,'Leucine':131.2,'Lysine':146.2,'Methionine':149.2,'Phenylalanine':165.2,
              'Proline':115.1,'Serine':105.1,'Threonine':119.1,'Tryptophan':204.2,'Tyrosine':181.2,'Valine':117.1}
  ##AA dic
self.aaC = {
        'A': 0, 'C': 0,'D': 0, 'E': 0,'F': 0,'G': 0, 'H': 0, 
        'I': 0,'J':0,'K': 0, 'L': 0, 'M': 0,'N':0,'P': 0, 
        'Q': 0,'R': 0,'S': 0,'T': 0, 'V': 0, 'Y': 0,'W': 0,'-': 0,
        }
        #Codon Composition Dic can use to add the codons to do the math
self.nucComp = {'A': 0, 'C': 0, 'G': 0, 'T': 0, 'N': 0, 'U': 0}
    self.codonComp= {
    'UUU': 0, 'UCU': 0, 'UAU': 0, 'UGU': 0,  
    'UUC': 0, 'UCC': 0, 'UAC': 0, 'UGC': 0,  
    'UUA': 0, 'UCA': 0, 'UAA': 0, 'UGA': 0,  
    'UUG': 0, 'UCG': 0, 'UAG': 0, 'UGG': 0,  
  
    'CUU': 0, 'CCU': 0, 'CAU': 0, 'CGU': 0,  
    'CUC': 0, 'CCC': 0, 'CAC': 0, 'CGC': 0,  
    'CUA': 0, 'CCA': 0, 'CAA': 0, 'CGA': 0,  
    'CUG': 0, 'CCG': 0, 'CAG': 0, 'CGG': 0,  
    'AUU': 0, 'ACU': 0, 'AAU': 0, 'AGU': 0,  
    'AUC': 0, 'ACC': 0, 'AAC': 0, 'AGC': 0,  
    'AUA': 0, 'ACA': 0, 'AAA': 0, 'AGA': 0,  
    'AUG': 0, 'ACG': 0, 'AAG': 0, 'AGG': 0,  
   
    'GUU': 0, 'GCU': 0, 'GAU': 0, 'GGU': 0,  
    'GUC': 0, 'GCC': 0, 'GAC': 0, 'GGC': 0,  
    'GUA': 0, 'GCA': 0, 'GAA': 0, 'GGA': 0,  
    'GUG': 0, 'GCG': 0, 'GAG': 0, 'GGG': 0}  
rnaCodonTable = {
    # RNA codon table
    # U
    'UUU': 'F', 'UCU': 'S', 'UAU': 'Y', 'UGU': 'C',  # UxU
    'UUC': 'F', 'UCC': 'S', 'UAC': 'Y', 'UGC': 'C',  # UxC
    'UUA': 'L', 'UCA': 'S', 'UAA': '-', 'UGA': '-',  # UxA
    'UUG': 'L', 'UCG': 'S', 'UAG': '-', 'UGG': 'W',  # UxG
    # C
    'CUU': 'L', 'CCU': 'P', 'CAU': 'H', 'CGU': 'R',  # CxU
    'CUC': 'L', 'CCC': 'P', 'CAC': 'H', 'CGC': 'R',  # CxC
    'CUA': 'L', 'CCA': 'P', 'CAA': 'Q', 'CGA': 'R',  # CxA
    'CUG': 'L', 'CCG': 'P', 'CAG': 'Q', 'CGG': 'R',  # CxG
    # A
    'AUU': 'I', 'ACU': 'T', 'AAU': 'N', 'AGU': 'S',  # AxU
    'AUC': 'I', 'ACC': 'T', 'AAC': 'N', 'AGC': 'S',  # AxC
    'AUA': 'I', 'ACA': 'T', 'AAA': 'K', 'AGA': 'R',  # AxA
    'AUG': 'M', 'ACG': 'T', 'AAG': 'K', 'AGG': 'R',  # AxG
    # G
    'GUU': 'V', 'GCU': 'A', 'GAU': 'D', 'GGU': 'G',  # GxU
    'GUC': 'V', 'GCC': 'A', 'GAC': 'D', 'GGC': 'G',  # GxC
    'GUA': 'V', 'GCA': 'A', 'GAA': 'E', 'GGA': 'G',  # GxA
    'GUG': 'V', 'GCG': 'A', 'GAG': 'E', 'GGG': 'G'  # GxG
    }
dnaCodonTable = {key.replace('U','T'):value for key, value in rnaCodonTable.items()}    
dic = {}
for key in AANameTo1Dic.keys():
    dic[AANameTo1Dic[key]] = key

print(dic)
