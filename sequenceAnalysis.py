#!/usr/bin/env python3
# Name: ajbarbee (CATS account username)
# Group Members: None

import sys
import os

def compliment(seq):
    '''Finds compliment of a DNA sequence'''
    seq = seq.replace("A","t") ; seq = seq.replace("T","a") ; seq = seq.replace("C","g") ; seq = seq.replace("G","c") 
    seq = seq.upper()
    return seq

def rev(seq):
    '''Finds the reverse of a DNA sequence'''
    leng = len(seq)
    rev = '' ; count = leng - 1
    for i in range(0,leng):
        rev += seq[count]
        count -= 1
    return rev


class FastAreader :
    ''' 
    Define objects to read FastA files.
    
    instantiation: 
    thisReader = FastAreader ('testTiny.fa')
    usage:
    for head, seq in thisReader.readFasta():
        print (head,seq)
    '''
    def __init__ (self, fname=''):
        '''contructor: saves attribute fname '''
        self.fname = fname
            
    def doOpen (self):
        ''' Handle file opens, allowing STDIN.'''
        if self.fname == '':
            return sys.stdin
        else:
            return open(self.fname)
        
    def readFasta (self):
        ''' Read an entire FastA record and return the sequence header/sequence'''
        header = ''
        sequence = ''
        
        with self.doOpen() as fileH:
            
            header = ''
            sequence = ''
            
            # skip to first fasta header
            line = fileH.readline()
            while not line.startswith('>') :
                line = fileH.readline()
            header = line[1:].rstrip()

            for line in fileH:
                if line.startswith ('>'):
                    yield header,sequence
                    header = line[1:].rstrip()
                    sequence = ''
                else :
                    sequence += ''.join(line.rstrip().split()).upper()

        yield header,sequence


class NucParams:
    rnaCodonTable = {
    # RNA codon table
    # U
    'UUU': 'F', 'UCU': 'S', 'UAU': 'Y', 'UGU': 'C',  # UxU
    'UUC': 'F', 'UCC': 'S', 'UAC': 'Y', 'UGC': 'C',  # UxC
    'UUA': 'L', 'UCA': 'S', 'UAA': '-', 'UGA': '-',  # UxA
    'UUG': 'L', 'UCG': 'S', 'UAG': '-', 'UGG': 'W',  # UxG
    # C
    'CUU': 'L', 'CCU': 'P', 'CAU': 'H', 'CGU': 'R',  # CxU
    'CUC': 'L', 'CCC': 'P', 'CAC': 'H', 'CGC': 'R',  # CxC
    'CUA': 'L', 'CCA': 'P', 'CAA': 'Q', 'CGA': 'R',  # CxA
    'CUG': 'L', 'CCG': 'P', 'CAG': 'Q', 'CGG': 'R',  # CxG
    # A
    'AUU': 'I', 'ACU': 'T', 'AAU': 'N', 'AGU': 'S',  # AxU
    'AUC': 'I', 'ACC': 'T', 'AAC': 'N', 'AGC': 'S',  # AxC
    'AUA': 'I', 'ACA': 'T', 'AAA': 'K', 'AGA': 'R',  # AxA
    'AUG': 'M', 'ACG': 'T', 'AAG': 'K', 'AGG': 'R',  # AxG
    # G
    'GUU': 'V', 'GCU': 'A', 'GAU': 'D', 'GGU': 'G',  # GxU
    'GUC': 'V', 'GCC': 'A', 'GAC': 'D', 'GGC': 'G',  # GxC
    'GUA': 'V', 'GCA': 'A', 'GAA': 'E', 'GGA': 'G',  # GxA
    'GUG': 'V', 'GCG': 'A', 'GAG': 'E', 'GGG': 'G'  # GxG
    }
    dnaCodonTable = {key.replace('U','T'):value for key, value in rnaCodonTable.items()}

    def __init__ (self, inString=''):
        """Initialize 4 Dictionaries.
        nucComp (nucleotide composition) stores the number of each nucleotide
        dnaCodonComp stores the number of every DNA codon
        rnaCodonComp stores the number of every RNA codon
        aaComp stores the number of every amino acid"""
        self.DNASeq = '' ; self.ProteinSeq = ''
        self.nucComp = {'A' : 0 , 'T' : 0 , 'C' : 0 , 'G' : 0 , 'U' : 0 , 'N' : 0}
        self.dnaCodonComp = {} ; self.rnaCodonComp = {} ;
        for codon in (self.dnaCodonTable.keys()):
            self.dnaCodonComp[codon] = 0
        for codon in (self.rnaCodonTable.keys()):
            self.rnaCodonComp[codon] = 0
        self.aaComp = {
        'A': 0,  'G': 0,  'M': 0, 'S': 0, 'C': 0,
        'H': 0, 'N': 0, 'T': 0, 'D': 0, 'I': 0,
        'P': 0, 'V': 0, 'E': 0, 'K': 0, 'Q': 0,
        'W': 0,  'F': 0, 'L': 0, 'R': 0, 'Y': 0 , '-' : 0 
        }
        self.addSequence(inString)
        
    def addSequence (self, inSeq):

        #Capitllize every letter, remove spaces,
        inSeq = inSeq.upper(); inSeq = inSeq.replace(" ","") ; ReadingRNA = True ; ReadingDNA = False
        #The next lines decide whether the function is reading RNA or DNA
        if inSeq.find('T') != -1:
            ReadingRNA = False ; ReadingDNA = True
        elif inSeq.find('U') != -1:
            ReadingRNA = True ; ReadingDNA = False
        Frst_3 = '' ; sequence = []
        #the while loop takes the first three letters of the inSeq and appends it in the list. This is reiterated
        # until nothing is left in inSeq, thus, all  codons are recorded.
        while (inSeq[0:3] != '' and (len(inSeq[0:3])>=3)):
            Frst_3 = inSeq[0:3]
            self.nucComp[Frst_3[0]] += 1 ; self.nucComp[Frst_3[1]] += 1 ; self.nucComp[Frst_3[2]] += 1
            sequence.append(Frst_3)
            inSeq = inSeq[3:]
        for codon in sequence:  #For each codon in the newly created list, we add 1 to its entry in the codon dictionary
            # For each codon in the newly created list,we find which amino acid it is, and we add 1 to its entry in the amino acid dictionary
            if codon.find('N') != -1: #ignore codons with N's
                pass
            elif ReadingRNA:
                self.DNASeq+=codon
                self.ProteinSeq+=self.rnaCodonTable[codon]
                self.rnaCodonComp[codon] += 1
                self.aaComp[self.rnaCodonTable[codon]] += 1
            else:
                self.DNASeq += codon
                self.ProteinSeq += self.dnaCodonTable[codon]
                self.dnaCodonComp[codon] += 1
                self.aaComp[self.dnaCodonTable[codon]] += 1
            
    def aaComposition(self):
        #returns amino acid dictionary, which was created in addSeq function
        return self.aaComp
    
    def nucComposition(self):
        #returns nucleotide dictionary, which was created in addSeq function
        return self.nucComp
    
    def codonComposition(self):
        #returns codon dictionary, which was created in addSeq function
        return self.codonComp
    
    def nucCount(self):
        #returns the count of nucleotides by adding each entry of the nucleotide dictionary
        Sum = 0
        for nuc in self.nucComp:
            Sum += self.nucComp[nuc]
        return Sum

class ProteinParam :
# These tables are for calculating:
#     molecular weight (aa2mw), along with the mol. weight of H2O (mwH2O)
#     absorbance at 280 nm (aa2abs280)
#     pKa of positively charged Amino Acids (aa2chargePos)
#     pKa of negatively charged Amino acids (aa2chargeNeg)
#     and the constants aaNterm and aaCterm for pKa of the respective termini
#  Feel free to move these to appropriate methods as you like

# As written, these are accessed as class attributes, for example:
# ProteinParam.aa2mw['A'] or ProteinParam.mwH2O

    aa2mw = { #Dictionary Containing the Molecular Weights of Amino Acids
        'A': 89.093,  'G': 75.067,  'M': 149.211, 'S': 105.093, 'C': 121.158,
        'H': 155.155, 'N': 132.118, 'T': 119.119, 'D': 133.103, 'I': 131.173,
        'P': 115.131, 'V': 117.146, 'E': 147.129, 'K': 146.188, 'Q': 146.145,
        'W': 204.225,  'F': 165.189, 'L': 131.173, 'R': 174.201, 'Y': 181.189
        }

    mwH2O = 18.015 #Molecular Weight of Water
    aa2abs280= {'Y':1490, 'W': 5500, 'C': 125}

    aa2chargePos = {'K': 10.5, 'R':12.4, 'H':6} #Positive Charge of certain Amino Acids
    aa2chargeNeg = {'D': 3.86, 'E': 4.25, 'C': 8.33, 'Y': 10} #Negative Charge of certain Amino Acids
    aaNterm = 9.69 #Nirtogen Terminus Weight
    aaCterm = 2.34 #C Terminous Weight

    def __init__ (self, protein):
        self.aaTable = {} #This holds the number of each Amino Acid in the protein
        for aa in self.aa2mw.keys():
            self.aaTable[aa] = 0
        for aa in protein:
            if aa in self.aaTable:
                self.aaTable[aa] += 1     

    def aaCount (self):
        aaCount = 0
        for aa in self.aaTable:
            aaCount += self.aaTable[aa]
        return aaCount

    def pI (self,precision=0.001):
        Fst = 0.0 ; Lst = 14.0 ; Mid = 7.0 ; pH = True
        while pH:
            if((self._charge_(Mid)) > 0-precision) and ((self._charge_(Mid)) < 0+precision):
                pH = False
                return Mid
            elif (self._charge_(Mid)) > 0 :
                Fst = Mid ; Mid = (Fst+Lst)/2
            elif (self._charge_(Mid)) < 0 :
                Lst = Mid ; Mid = (Fst+Lst)/2
                

    def aaComposition (self) :
        return self.aaTable

    def _charge_ (self,pH):
        positive = ((10**self.aaNterm)/((10**self.aaNterm)+10**pH)) ; negative = ((10**pH)/((10**self.aaCterm)+10**pH)) ; netCharge = 0
        for aa in self.aaTable:
            if aa in {'R','K','H'}: #Count Positive Charges
                positive += self.aaTable[aa]*((10**self.aa2chargePos[aa])/((10**self.aa2chargePos[aa])+10**pH))
        for aa in self.aaTable:
            if aa in {'D','E','C','Y'}: #Count Negative Charges
                negative += self.aaTable[aa]*((10**pH)/((10**self.aa2chargeNeg[aa])+10**pH))
        netCharge = positive - negative
        return netCharge

    def molarExtinction (self):
        molarExtinction = 0 
        for aa in {'Y','W','C'}:
            molarExtinction += self.aaTable[aa]*self.aa2abs280[aa]
        return molarExtinction
            

    def massExtinction (self):
        myMW =  self.molecularWeight()
        return self.molarExtinction() / myMW if myMW else 0.0

    def molecularWeight (self):
        molecularWeight = 0
        for aa in self.aaTable:
            molecularWeight += self.aa2mw[aa]*self.aaTable[aa]
        molecularWeight -= (self.aaCount()-1)*self.mwH2O
        return molecularWeight

class ORF_Finder:
    '''Finds the ORFS in a fafsa file. Will either give you all the punative ORFS or just the longest one.
        The minimm gene size is able to be chosen. '''
    def __init__(self,lG,S,T,Mini,eG, pG, pP, MWmax, MWmin):
        #first few lines set parameters
        OnlyLngGene = lG
        STRT = S
        END = T
        Min = Mini
        EveryGene = eG
        printGene = pG
        printProtein = pP
        MWminimum = MWmin
        MWmaximum = MWmax
        myReader = FastAreader() # make sure to change this to use stdin
        sequence = '' ;  Headers = []  
        for head, seq in myReader.readFasta() :
            sequence = seq.replace(' ','') #clean up sequence
            #initialize everything
            ORF_List = [] ; leng = len(sequence) ; p = 0 ; codon = '' ; Strt_List = []
            ORF_Loc_Dic = {} ; Orf_Lgth_Dic = {} ; Dsply_Pos_Dic = {} ; comp = 0 ;  No_Stp_Yet = True
            ORF_Seq_Dic = {}
            for frame in [+1,+2,+3]:
                if (frame == +2 or frame == +3): 
                    sequence = sequence[1:] #remove first nucleotide to change reading frame
                    comp += 1
                while (p < leng):
                    codon = sequence[p:(p+3)] #read codon
                    if (codon in STRT):
                        Strt_List.append(p) #keeps track of all the starts
                    elif (codon in END):
                        if (No_Stp_Yet and (not(sequence[0:3] in STRT))): #takes note of gene fragment if present
                            if (frame == 1):
                                ORF_Loc_Dic[-comp] = (p+3)
                                ORF_Seq_Dic[-comp] = sequence[-comp:(p+3)]
                            if (frame == 2):
                                ORF_Loc_Dic[-comp] = (p+3+comp)
                                ORF_Seq_Dic[-comp] = sequence[-comp:(p+3)]
                            if (frame == 3):
                                ORF_Loc_Dic[-comp] = (p+3+comp)
                                ORF_Seq_Dic[-comp] = sequence[-comp:(p+3)]
                        No_Stp_Yet = False
                        if EveryGene:    
                            for pos in Strt_List: #all punative genes are recorded
                                ORF_Loc_Dic[pos] = (p+3+comp)
                                ORF_Seq_Dic[pos] = sequence[pos:(p+3)]

                        else:
                            if (Strt_List != []): #only largest punative gene is recorded
                                ORF_Loc_Dic[min(Strt_List)] = (p+3+comp)
                                ORF_Seq_Dic[min(Strt_List)] = sequence[min(Strt_List):(p+3)]
                        Strt_List = []

                    elif ((p+3)>=len(sequence)):
                        for pos in Strt_List:
                            ORF_Loc_Dic[pos] = leng
                            ORF_Seq_Dic[pos] = sequence[pos:leng]
                        Strt_List = []
                    p += 3
                for pos in ORF_Loc_Dic.keys():
                    Orf_Lgth_Dic[pos] = int(ORF_Loc_Dic[pos]-comp-pos) #finds length of ORF
                for pos in ORF_Loc_Dic.keys():
                    Dsply_Pos_Dic[pos] = pos + 1 + comp #find location to display for each start
                for pos in ORF_Loc_Dic.keys(): #adds ORF to ORF list if it's big enough
                    if (Orf_Lgth_Dic[pos]>=Min):
                        ORF_List.append([frame,Dsply_Pos_Dic[pos],ORF_Loc_Dic[pos],Orf_Lgth_Dic[pos], ORF_Seq_Dic[pos]])
                #reset all variables for next frame
                p = 0 ; codon = '' ; Strt_List = [] ; ORF_Loc_Dic = {} ; Orf_Lgth_Dic = {} ; Dsply_Pos_Dic = {} ; ORF_Seq_Dic = {}

            #repeat everything for the - strand
            sequence = seq.replace(' ','') ; sequence = compliment(sequence)
            p = len(sequence) ; codon = '' ; Strt_List = [] ; ORF_Loc_Dic = {} ; Orf_Lgth_Dic = {} ; Dsply_Pos_Dic = {} ; comp = 1 ; No_Stp_Yet = True
            ORF_Seq_Dic = {}
            InStart = False
            for frame in [-1,-2,-3]:
                if (frame == -2 or frame == -3): 
                    sequence = sequence[0:(len(sequence))-1] #reset the sequence
                    p = len(sequence)
                while (p > 0):
                    codon = sequence[(p-3):(p)]
                    if (rev(codon) in STRT):    #looks at the reverse of each start codon
                        ORFSeq = ''
                        InStart = True
                        Strt_List.append(p)
                    elif (rev(codon) in END): #looks at reverse stop codon
                        InStart = False
                        if (No_Stp_Yet and (not(rev(sequence[(len(sequence)-3):len(sequence)]) in STRT))):
                            if (frame == -1):
                                ORF_Loc_Dic[len(sequence)] = (p-2)
                                ORF_Seq_Dic[len(sequence)] = rev(sequence[(p-2):len(sequence)])
                            if (frame == -2):
                                ORF_Loc_Dic[len(sequence)+1] = (p-2)
                                ORF_Seq_Dic[len(sequence)] = rev(sequence[(p - 2):len(sequence)+1])
                            if (frame == -3):
                                ORF_Loc_Dic[len(sequence)+2] = (p-2)
                                ORF_Seq_Dic[len(sequence)] = rev(sequence[(p - 2):len(sequence)+2])
                        No_Stp_Yet = False
                        if EveryGene:    
                            for pos in Strt_List:
                                ORF_Loc_Dic[pos] = (p-3+comp)
                                ORF_Seq_Dic[pos] = rev(sequence[(p-3):pos])
                        else:
                            if (Strt_List != []):
                                ORF_Loc_Dic[max(Strt_List)] = (p-3+comp)
                                ORF_Seq_Dic[max(Strt_List)] = rev(sequence[(p-3):max(Strt_List)])
                        Strt_List = []
                    elif ((p-3)<=0):
                        for pos in Strt_List:
                            ORF_Loc_Dic[pos] = 1
                            ORF_Seq_Dic[pos] = rev(sequence[1:pos])
                        Strt_List = []
                    p -= 3 #p is decreased instead of increased
                for pos in ORF_Loc_Dic.keys():
                    Orf_Lgth_Dic[pos] = int((ORF_Loc_Dic[pos]-pos-comp)*-1)
                for pos in ORF_Loc_Dic.keys():
                    Dsply_Pos_Dic[pos] = pos + 1 - comp
                for pos in ORF_Loc_Dic.keys():
                    if (Orf_Lgth_Dic[pos]>=Min):
                        ORF_List.append([frame,ORF_Loc_Dic[pos],Dsply_Pos_Dic[pos],Orf_Lgth_Dic[pos],ORF_Seq_Dic[pos]])
                codon = '' ; Strt_List = [] ; ORF_Loc_Dic = {} ; Orf_Lgth_Dic = {} ; Dsply_Pos_Dic = {} ; ORF_Seq_Dic = {}

            print("{}\n".format(head)) #writes the header
            Srted_ORF_List = sorted(ORF_List, key=lambda x: x[3],reverse=True) #sorts the ORF list by size
            if OnlyLngGene:
                if Srted_ORF_List == []: #only displays longest ORF if choosen
                    print("There are no ORFS at this minimum gene cut-off\n")

                else: #adds each ORF to the text document
                    print("The Longest ORF is:\n{:+d} {:>5d}..{:>5d} {:>5d}\n".format(Srted_ORF_List[0][0],Srted_ORF_List[0][1],Srted_ORF_List[0][2],Srted_ORF_List[0][3]))

            else:
                if Srted_ORF_List == []:
                    print('There are no ORFS at this minimum gene cut-off\n')
                else:
                    for orf in Srted_ORF_List:
                        print(("{:+d} {:>5d}..{:>5d} {:>5d}\n".format(orf[0],orf[1],orf[2],orf[3])))


                if(printGene):
                    filename = "ORFSequences.txt"
                    f = open(filename, "a")
                    f.write("{}\n".format(head))  # writes the header
                    for orf in Srted_ORF_List:
                        f.write("{:+d} {:>5d}..{:>5d} {:>5d}\n".format(orf[0], orf[1], orf[2], orf[3]))
                        f.write(orf[4])
                        f.write('\n')
                    f.close()

                if (printProtein):
                    filename = "ORFProteins.txt"
                    f = open(filename, "a")
                    f.write("{}\n".format(head))  # writes the header
                    for orf in Srted_ORF_List:
                        ThisProtein = NucParams(orf[4])
                        f.write("{:+d} {:>5d}..{:>5d} {:>5d}\n".format(orf[0], orf[1], orf[2], orf[3]))
                        f.write(ThisProtein.ProteinSeq)
                        f.write('\n')
                    f.close()

                if (MWminimum != None and MWmaximum != None):
                    DesiredORFs = {}
                    for orf in Srted_ORF_List:
                        ThisProtein = NucParams(orf[4])
                        ThisMW = ProteinParam(ThisProtein.ProteinSeq).molecularWeight()
                        #print('The MW is: ', ThisMW)
                        if( ThisMW >= MWminimum and ThisMW <= MWmaximum):
                            DesiredORFs[(orf[0], orf[1], orf[2], orf[3], ThisMW)] = ThisProtein.ProteinSeq


                    ORFsList = []
                    for ORF in DesiredORFs.keys():
                        ORFsList.append(ORF)
                    SortedORFsList = sorted( ORFsList, key=lambda x: x[4] )
                    filename = "ORFMolecularWeights.txt"
                    f = open(filename, "a")
                    f.write("{}\n".format(head))
                    if (len(SortedORFsList)==0):
                        f.write('There are no Proteins in this Molecular Weight range')
                    else:
                        for DesiredORF in SortedORFsList:
                            f.write("{:+d} {:>5d}..{:>5d} {:>5d}\n".format(DesiredORF[0], DesiredORF[1], DesiredORF[2], DesiredORF[3]))
                            f.write("The Molecular Weight is: {:>5d}\n".format(int(DesiredORF[4])))
                            f.write(DesiredORFs[DesiredORF])
                            f.write('\n')
                        f.close()
