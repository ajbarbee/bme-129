AAToNumDic = {'Alanine': 0, 'Arginine': 1, 'Asparagine': 2, 'Aspartate': 3, 'Cysteine': 4, 'Glutamate': 5,
              'Glutamine': 6, 'Glycine': 7, 'Histidine': 8, 'Isoleucine': 9, 'Leucine': 10, 'Lysine': 11,
              'Methionine': 12, 'Phenylalanine': 13, 'Proline': 14, 'Serine': 15, 'Threonine': 16,
              'Tryptophan': 17, 'Tyrosine': 18, 'Valine': 19}


NumToAADic = {0: 'Alanine', 1: 'Arginine', 2: 'Asparagine', 3: 'Aspartate', 4: 'Cysteine', 5: 'Glutamate',
              6: 'Glutamine', 7: 'Glycine', 8: 'Histidine', 9: 'Isoleucine', 10: 'Leucine', 11: 'Lysine',
              12: 'Methionine', 13: 'Phenylalanine', 14: 'Proline', 15: 'Serine', 16: 'Threonine',
              17: 'Tryptophan', 18: 'Tyrosine', 19: 'Valine'}


AANameTo3Dic = {'Alanine':'Ala', 'Arginine':'Arg','Asparagine':'Asn','Aspartate':'Asp','Cysteine':'Cys','Glutamate':'Glu','Glutamine':'Gln',
              'Glycine':'Gly','Histidine':'His','Isoleucine':'Ile','Leucine':'Leu','Lysine':'Lys','Methionine':'Met','Phenylalanine':'Phe',
              'Proline':'Pro','Serine':'Ser','Threonine':'Thr','Tryptophan':'Trp','Tyrosine':'Tyr','Valine':'Val'}

AA3ToNameDic = {'Ala': 'Alanine', 'Arg': 'Arginine', 'Asn': 'Asparagine', 'Asp': 'Aspartate', 'Cys': 'Cysteine', 'Glu': 'Glutamate', 'Gln': 'Glutamine',
                'Gly': 'Glycine', 'His': 'Histidine', 'Ile': 'Isoleucine', 'Leu': 'Leucine', 'Lys': 'Lysine', 'Met': 'Methionine', 'Phe': 'Phenylalanine',
                'Pro': 'Proline', 'Ser': 'Serine', 'Thr': 'Threonine', 'Trp': 'Tryptophan', 'Tyr': 'Tyrosine', 'Val': 'Valine'}

AANameTo1Dic = {'Alanine':'A', 'Arginine':'R','Asparagine':'N','Aspartate':'D','Cysteine':'C','Glutamate':'E','Glutamine':'Q',
              'Glycine':'G','Histidine':'H','Isoleucine':'I','Leucine':'L','Lysine':'K','Methionine':'M','Phenylalanine':'F',
              'Proline':'P','Serine':'S','Threonine':'T','Tryptophan':'W','Tyrosine':'Y','Valine':'V'}

AA1ToNameDic = {'A': 'Alanine', 'R': 'Arginine', 'N': 'Asparagine', 'D': 'Aspartate', 'C': 'Cysteine', 'E': 'Glutamate', 'Q': 'Glutamine',
                'G': 'Glycine', 'H': 'Histidine', 'I': 'Isoleucine', 'L': 'Leucine', 'K': 'Lysine', 'M': 'Methionine', 'F': 'Phenylalanine',
                'P': 'Proline', 'S': 'Serine', 'T': 'Threonine', 'W': 'Tryptophan', 'Y': 'Tyrosine', 'V': 'Valine'}

AAWeightDic = {'Alanine':89.1, 'Arginine':174.2,'Asparagine':132.1,'Aspartate':133.1,'Cysteine':121.2,'Glutamate':147.1,'Glutamine':146.2,
              'Glycine':75.1,'Histidine':155.2,'Isoleucine':131.2,'Leucine':131.2,'Lysine':146.2,'Methionine':149.2,'Phenylalanine':165.2,
              'Proline':115.1,'Serine':105.1,'Threonine':119.1,'Tryptophan':204.2,'Tyrosine':181.2,'Valine':117.1}

EmptyCompDic = {'A': 0, 'R': 0, 'N': 0, 'D': 0, 'C': 0, 'E': 0, 'Q': 0,
                  'G': 0, 'H': 0, 'I': 0, 'L': 0, 'K': 0, 'M': 0, 'F': 0,
                  'P': 0, 'S': 0, 'T': 0, 'W': 0, 'Y': 0, 'V': 0}

def FindProteinWeight(CompList):
    """Finds the weight of a given protein using its number of amino acids"""
    Weight = 0.0
    for i in range(20):
        Weight += CompList[i]*AAWeightDic[NumToAADic[i]] #use name dictionary to convert from single letter to name
    return Weight

CompDic = {'A': 1, 'R': 1, 'N': 0, 'D': 1, 'C': 0, 'E': 0, 'Q': 11,
            'G': 1, 'H': 3, 'I': 4, 'L': 2, 'K': 0, 'M': 0, 'F': 2,
            'P': 4, 'S': 0, 'T': 0, 'W': 0, 'Y': 5, 'V': 0}

CompList = [1,1,0,1,0,0,11,1,3,4,2,0,0,2,4,0,0,0,5,0]

print(FindProteinWeight(CompList))

