#!/usr/bin/env python3
# Name: ajbarbee (CATS account username)
# Group Members: None


########################################################################
#Read Fasta file containing sequence with FASTA reader and transfer it to working memory
            #Clean up sequence if needed
            #Initialize variables and list. We will have a list to keep track of each start location (according to python), and three dictionaries
            #that will store each starts' nearest stop codon, the length of the punative gene, and the location that the user will see (First postision is 1 not 0)
            #we also initialize the variable p, which is the position we are currently looking at, and codon, the codon we are currently looking at, 
            #and comp, which helps compensating in calculations when we frame shift
            #for loop that happens three times, one for each reading frame on the positive strand
                #initialize variable that tells whether we have seen a first stop codon yet to false. Helps find boundary conditions
                #an if function that deletes the first nucleotide at the begining of the sequence, thus changing reading frames
                #begin a while loop that repeats for as long as p is less than the size of the sequence
                    #codon is set to the next triplet, first if we just started
                        #if the codon is a start codon, append it to the list of start codons
                        #if it is a stop codon:
                            #we have a gene fragment we record if we haven't run into any start codons yet
                            #we set the stopping position to be p for every start on the start list, 
                            #if we seek every punative gene, or set p to be the end for the start that gives us the largest ORF     
                            #we then empty the start list
                        #if codon is less than three characters, we know we are at the end of the gene, so we set this as the ending
                        #position for the farthest away start codon
                        #if the codon is neither a start or stop, we simply move p over by three and look at the next triplet
                    #We now know the start and stop position for each ORF, so we now find the length of each ORF by finding the difference
                    #The display position is found by adding one to the positon python thinks its in, and add 'comp' if we deleted the first nucleotide
                    #if the ORF is longer than our minimum gene length, it is added to our ORF list
                    #all lists, variables and dictionaries are initialized except the ORF list


            #regenenetate the orginal sequence, clean it up, and find its compliment
            #We reinitialize all the same variables as before, expcept that p starts at the end of the sequence
            #We then do another for loop the reiterates for each of the reading frames on the negative strand
                #the code is nearly identical as that for the + strand but we read begining from the end of the sequence and search
                #for the reverse of each start/stop codon
                #the ORFS are added if they are above the minimum gene size
            #the program then creates a new text file and appends the header we are currently under, and will add the ORF list, ordered by descending size
#Main takes the commandline parameters and inserts them into the program
'''
We expect the input to be a DNA sequence of only A,G,C, and T
The expected input of the file is a fasta file and the expected output is a text file with the ORFs listed in descending order such as:
+1 57166..61908  4743
-1  8192..11422  3231
+2 65963..69004  3042
-3 14589..16862  2274
-2  2968.. 4872  1905
+1 64093..65952  1860

'''
########################################################################
import sequenceAnalysis as sa

def compliment(seq):
    '''Finds compliment of a DNA sequence'''
    seq = seq.replace("A","t") ; seq = seq.replace("T","a") ; seq = seq.replace("C","g") ; seq = seq.replace("G","c") 
    seq = seq.upper()
    return seq

def rev(seq):
    '''Finds the reverse of a DNA sequence'''
    leng = len(seq)
    rev = '' ; count = leng - 1
    for i in range(0,leng):
        rev += seq[count]
        count -= 1
    return rev

class CommandLine() :
    '''
    Handle the command line, usage and help requests.

    CommandLine uses argparse, now standard in 2.7 and beyond. 
    it implements a standard command line argument parser with various argument options,
    a standard usage and help.

    attributes:
    all arguments received from the commandline using .add_argument will be
    avalable within the .args attribute of object instantiated from CommandLine.
    For example, if myCommandLine is an object of the class, and requiredbool was
    set as an option using add_argument, then myCommandLine.args.requiredbool will
    name that option.
 
    '''
    
    def __init__(self, inOpts=None) :
        '''
        Implement a parser to interpret the command line argv string using argparse.
        '''
        
        import argparse
        self.parser = argparse.ArgumentParser(description = 'Program prolog - a brief description of what this thing does', 
                                             epilog = 'Program epilog - some other stuff you feel compelled to say', 
                                             add_help = True, #default is True 
                                             prefix_chars = '-', 
                                             usage = '%(prog)s [options] -option1[default] <input >output'
                                             )
        self.parser.add_argument('-lG', '--longestGene', action = 'store', nargs='?', const=True, default=False, help='longest Gene in an ORF')
        self.parser.add_argument('-mG', '--minGene', type=int, default=100, action = 'store', help='minimum Gene length')
        self.parser.add_argument('-s', '--start', action = 'append', default = ['ATG'],nargs='?', 
                                 help='start Codon') #allows multiple list options
        self.parser.add_argument('-eG', '--everyGene', type=bool, nargs='?', const=True, default=False, action = 'store', help='True if every punative gene is desired')
        self.parser.add_argument('-pG', '--printGenes', type=bool, nargs='?', const=True, default=False, action='store', help='True if you desire to see the Seqs of each ORF')
        self.parser.add_argument('-pP', '--printProteins', type=bool, nargs='?', const=True, default=False, action='store',
                                 help='True if you desire to see the aa seq of each ORF')
        self.parser.add_argument('-MWmax', '--MolecularWeightMaximum', type=int, default=None, action='store', help='minimum Gene length')
        self.parser.add_argument('-MWmin', '--MolecularWeightMinimum', type=int, default=None, action='store',
                                 help='minimum Gene length')
        self.parser.add_argument('-t', '--stop', action = 'append', default = ['TAG','TGA','TAA'],nargs='?', help='stop Codon') #allows multiple list options
        self.parser.add_argument('-v', '--version', action='version', version='%(prog)s 0.1')
        
        if inOpts is None :
            self.args = self.parser.parse_args()
        else :
            self.args = self.parser.parse_args(inOpts)

########################################################################
# Main
# Here is the main program
# Takes the paramets set in the commandline, and delivers them to the ORF finder class
#
########################################################################
   
def main(inFile = None, options = None):
    '''
    Call ORF finder to read DNA sequence and give ORFS in accordance to conditions set in commandline  
    '''
    
    thisCommandLine = CommandLine(options)
    #The next line sets the conditions
    ORFS = sa.ORF_Finder(thisCommandLine.args.longestGene,thisCommandLine.args.start,thisCommandLine.args.stop,thisCommandLine.args.minGene,thisCommandLine.args.everyGene,
                         thisCommandLine.args.printGenes,thisCommandLine.args.printProteins, thisCommandLine.args.MolecularWeightMaximum, thisCommandLine.args.MolecularWeightMinimum)
    
    ###### replace the code between comments.
    print (thisCommandLine.args) #prints parameters

    #######
    
if __name__ == "__main__":
    main() # delete this stuff if running from commandline
