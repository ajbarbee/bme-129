#!/usr/bin/env python3
########################################################################
# File:
# Executable:
# Purpose:
# Author: Alexie Barbee
# History: ajb 1/17/2023 Created
########################################################################
import sys
import AAData as AA
class FileReader:
    """
    This class reads a file.
    Implementation:
    inFile = 'file.txt'
    Reader = FileReader(inFile)
    for line in Reader.readFile():
        List.append(line) , String += line, etc.
    """
    def __init__(self, fname=''):
        '''Initialization saves attribute fname '''
        self.fname = fname

    def doOpen(self):
        """
        Returns the file to be opened, whether that be a specified file or Standard In
        """
        if self.fname == '':
            return sys.stdin
        else:
            return open(self.fname)

    def readFile(self):
        """
        Reads the file using doOpen method.
        """
        with self.doOpen() as fileH:
            for line in fileH: #return every character from a line in the document
                line = line[0:].rstrip()
                if (line != ''):
                    yield line
########################################################################
# The WorkHorse Class
########################################################################
class MWtoProtein:
    def __init__(self):
        pass

    def InterpretRawData(self):
        pass




def main(inFile=None):
    Reader = FileReader(inFile)

    for line in Reader.readFile():
        pass

if __name__ == "__main__":
    main(inFile='text.txt')