"""    
With Some minor tweaks this should return dictionaries with the amino acid count and the codon count 
could break it down futher to store postion data when it stores the codon dictonary as to store the count of codon and where

Please Refrence the Dictonary Storage File to see what dictionaries are at play
""" 
    
    def addSequence (self, inSeq):
        '''
        Want to go thrugh the nuclotide given and add to the dictionaries much in 
the same way
        that the nuclotides from the given FastA file were handled
        '''
        #similar to how the initial input is added so will the users
        Seq=inSeq.upper().replace(' ', "")#correct the input
        #DNA to Rna
        protein=""
        if len(Seq)%3==0:
            for i in range(0, len(Seq), 3):
                codon = Seq[i:i+3] #goes through the codon by 3
                protein+=NucParams.dnaCodonTable[codon]#adds the given codon to 
        list protein
        for aa in protein:
            if aa in self.aaC.keys():
                self.aaC[aa] += 1 #starts counting the AA in the lsit
        for nuc in Seq:
            if nuc in self.nucComp.keys():
                self.nucComp[nuc] += 1 #counts the diffent codon counts 
        RNAseq=Seq.replace('T','U')
    
        if len(RNAseq)%3==0:
            for i in range(0, len(RNAseq), 3):
                codon=RNAseq[i:i+3]
                if codon in NucParams.rnaCodonTable.keys():
                        self.codonComp[codon] += 1